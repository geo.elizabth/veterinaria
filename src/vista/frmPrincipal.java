/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

//import Controller.PersonaDB;
import Modelo.Persona;

/**
 *
 * @author Eli
 */
public class frmPrincipal extends javax.swing.JFrame {

//    PersonaDB persDB = new PersonaDB();
    String rol;
    String idUsuario;

    /**
     * Creates new form frmPrincipal
     */
    public frmPrincipal() {
        initComponents();
        this.setExtendedState(MAXIMIZED_BOTH);
        this.setLocationRelativeTo(this);
        new CambiaPanel(PanelCentral, new pnlHome());
//        rol = roles;
//        idUsuario = user;
        Permisos();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        panelLateral = new javax.swing.JPanel();
        jToggleButton1 = new javax.swing.JToggleButton();
        btnUsuario = new javax.swing.JToggleButton();
        btnCliente = new javax.swing.JToggleButton();
        btnMascota = new javax.swing.JToggleButton();
        btnReserva = new javax.swing.JToggleButton();
        btnPago = new javax.swing.JToggleButton();
        btnReporte = new javax.swing.JToggleButton();
        btnCerrarSecion = new javax.swing.JToggleButton();
        panelsuperior = new javax.swing.JPanel();
        lblRol = new javax.swing.JLabel();
        lblIcono = new javax.swing.JLabel();
        lblUsuario = new javax.swing.JLabel();
        jToolBar1 = new javax.swing.JToolBar();
        btnExit1 = new javax.swing.JButton();
        PanelCentral = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        panelLateral.setBackground(new java.awt.Color(255, 153, 153));
        panelLateral.setPreferredSize(new java.awt.Dimension(200, 470));

        buttonGroup1.add(jToggleButton1);
        jToggleButton1.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jToggleButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/icons8_Home_32px.png"))); // NOI18N
        jToggleButton1.setText("Inicio            ");
        jToggleButton1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jToggleButton1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jToggleButton1.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        jToggleButton1.setIconTextGap(10);
        jToggleButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton1ActionPerformed(evt);
            }
        });

        buttonGroup1.add(btnUsuario);
        btnUsuario.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btnUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/icons8_Male_User_32px_2.png"))); // NOI18N
        btnUsuario.setText("Usuario         ");
        btnUsuario.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnUsuario.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUsuarioActionPerformed(evt);
            }
        });

        buttonGroup1.add(btnCliente);
        btnCliente.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btnCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/icons8_User_Groups_32px.png"))); // NOI18N
        btnCliente.setText("Clientes        ");
        btnCliente.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnCliente.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClienteActionPerformed(evt);
            }
        });

        buttonGroup1.add(btnMascota);
        btnMascota.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btnMascota.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/User/icons8_dog_paw_25px.png"))); // NOI18N
        btnMascota.setText("Mascota          ");
        btnMascota.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnMascota.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnMascota.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMascotaActionPerformed(evt);
            }
        });

        buttonGroup1.add(btnReserva);
        btnReserva.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btnReserva.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/User/icons8_order_history_25px.png"))); // NOI18N
        btnReserva.setText("Historia Clinica");
        btnReserva.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnReserva.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnReserva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReservaActionPerformed(evt);
            }
        });

        buttonGroup1.add(btnPago);
        btnPago.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btnPago.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/User/icons8_doctors_folder_25px_1.png"))); // NOI18N
        btnPago.setText("Consulta      ");
        btnPago.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnPago.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnPago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPagoActionPerformed(evt);
            }
        });

        buttonGroup1.add(btnReporte);
        btnReporte.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btnReporte.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/Reporte.png"))); // NOI18N
        btnReporte.setText("Rerportes     ");
        btnReporte.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnReporte.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnReporte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReporteActionPerformed(evt);
            }
        });

        buttonGroup1.add(btnCerrarSecion);
        btnCerrarSecion.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btnCerrarSecion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/Ventana/icons8_Synchronize_14px.png"))); // NOI18N
        btnCerrarSecion.setText("Cerrar Sesión");
        btnCerrarSecion.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnCerrarSecion.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnCerrarSecion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarSecionActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelLateralLayout = new javax.swing.GroupLayout(panelLateral);
        panelLateral.setLayout(panelLateralLayout);
        panelLateralLayout.setHorizontalGroup(
            panelLateralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLateralLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelLateralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jToggleButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 192, Short.MAX_VALUE)
                    .addComponent(btnUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, 192, Short.MAX_VALUE)
                    .addComponent(btnCliente, javax.swing.GroupLayout.DEFAULT_SIZE, 192, Short.MAX_VALUE)
                    .addComponent(btnReserva, javax.swing.GroupLayout.DEFAULT_SIZE, 192, Short.MAX_VALUE)
                    .addComponent(btnPago, javax.swing.GroupLayout.DEFAULT_SIZE, 192, Short.MAX_VALUE)
                    .addComponent(btnReporte, javax.swing.GroupLayout.DEFAULT_SIZE, 192, Short.MAX_VALUE)
                    .addComponent(btnCerrarSecion, javax.swing.GroupLayout.DEFAULT_SIZE, 192, Short.MAX_VALUE)
                    .addComponent(btnMascota, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelLateralLayout.setVerticalGroup(
            panelLateralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLateralLayout.createSequentialGroup()
                .addComponent(jToggleButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnMascota, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnReserva, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnPago, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnReporte, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 23, Short.MAX_VALUE)
                .addComponent(btnCerrarSecion, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        getContentPane().add(panelLateral, java.awt.BorderLayout.LINE_START);

        panelsuperior.setBackground(new java.awt.Color(102, 102, 255));
        panelsuperior.setPreferredSize(new java.awt.Dimension(1008, 50));

        lblRol.setFont(new java.awt.Font("Dialog", 1, 20)); // NOI18N

        lblIcono.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblIcono.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/User/icons8-person-30.png"))); // NOI18N

        lblUsuario.setFont(new java.awt.Font("Dialog", 1, 20)); // NOI18N

        jToolBar1.setBackground(new java.awt.Color(24, 110, 214));
        jToolBar1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jToolBar1.setRollover(true);

        btnExit1.setFont(new java.awt.Font("Dialog", 1, 20)); // NOI18N
        btnExit1.setText("Salir");
        btnExit1.setFocusable(false);
        btnExit1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnExit1.setPreferredSize(new java.awt.Dimension(70, 35));
        btnExit1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnExit1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExit1ActionPerformed(evt);
            }
        });
        jToolBar1.add(btnExit1);

        javax.swing.GroupLayout panelsuperiorLayout = new javax.swing.GroupLayout(panelsuperior);
        panelsuperior.setLayout(panelsuperiorLayout);
        panelsuperiorLayout.setHorizontalGroup(
            panelsuperiorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelsuperiorLayout.createSequentialGroup()
                .addContainerGap(158, Short.MAX_VALUE)
                .addComponent(lblRol, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(120, 120, 120)
                .addComponent(lblIcono, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31)
                .addComponent(lblUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 317, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        panelsuperiorLayout.setVerticalGroup(
            panelsuperiorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelsuperiorLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelsuperiorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(lblRol, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblIcono, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getContentPane().add(panelsuperior, java.awt.BorderLayout.PAGE_START);

        PanelCentral.setLayout(new javax.swing.BoxLayout(PanelCentral, javax.swing.BoxLayout.LINE_AXIS));
        getContentPane().add(PanelCentral, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public void Permisos() {
//        if (rol.equals("G")) {
//            lblRol.setText("Administrador");
//        } else {
//            lblRol.setText("Recepcionista");
//            bloqueoUsuario();
//        }
//        Persona per = persDB.buscapersona(idUsuario);
//        lblUsuario.setText(per.getApell_persona() + " " + per.getNomb_persona());
    }

    public void bloqueoUsuario() {
        btnUsuario.setVisible(false);
        btnMascota.setVisible(false);
    }


    private void jToggleButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton1ActionPerformed
        new CambiaPanel(PanelCentral, new pnlHome());
    }//GEN-LAST:event_jToggleButton1ActionPerformed

    private void btnUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUsuarioActionPerformed
        new CambiaPanel(PanelCentral, new frmUsuario());
    }//GEN-LAST:event_btnUsuarioActionPerformed

    private void btnClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClienteActionPerformed
        frmCliente venCliente = new frmCliente();
        new CambiaPanel(PanelCentral, venCliente);
    }//GEN-LAST:event_btnClienteActionPerformed

    private void btnMascotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMascotaActionPerformed
        new CambiaPanel(PanelCentral, new frmMascota());
    }//GEN-LAST:event_btnMascotaActionPerformed

    private void btnReservaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReservaActionPerformed
//        new CambiaPanel(PanelCentral, new frmReserva());
    }//GEN-LAST:event_btnReservaActionPerformed

    private void btnPagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPagoActionPerformed
//        new CambiaPanel(PanelCentral, new frmPago());
    }//GEN-LAST:event_btnPagoActionPerformed

    private void btnExit1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExit1ActionPerformed
        System.exit(0);        // TODO add your handling code here:
    }//GEN-LAST:event_btnExit1ActionPerformed

    private void btnReporteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReporteActionPerformed
//        new CambiaPanel(PanelCentral, new frmReportes());
    }//GEN-LAST:event_btnReporteActionPerformed

    private void btnCerrarSecionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarSecionActionPerformed
        Inicio frmInicio = new Inicio();
        frmInicio.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnCerrarSecionActionPerformed

//    /**
//     * @param args the command line arguments
//     */
//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(frmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(frmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(frmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(frmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new frmPrincipal(null, null).setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel PanelCentral;
    private javax.swing.JToggleButton btnCerrarSecion;
    private javax.swing.JToggleButton btnCliente;
    private javax.swing.JButton btnExit1;
    private javax.swing.JToggleButton btnMascota;
    private javax.swing.JToggleButton btnPago;
    private javax.swing.JToggleButton btnReporte;
    private javax.swing.JToggleButton btnReserva;
    private javax.swing.JToggleButton btnUsuario;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JToggleButton jToggleButton1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JLabel lblIcono;
    private javax.swing.JLabel lblRol;
    private javax.swing.JLabel lblUsuario;
    private javax.swing.JPanel panelLateral;
    private javax.swing.JPanel panelsuperior;
    // End of variables declaration//GEN-END:variables
}
