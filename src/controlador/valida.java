package controlador;

import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Eli
 */
public class valida {

    //        Calendar c1 = GregorianCalendar.getInstance();
//        System.out.println("Fecha actual: " + c1.getTime().toLocaleString());
//        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMMMM/yyyy hh:mm:ss");
//        System.out.println("Fecha Formateada: "+sdf.format(c1.getTime()));
//        sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
//        System.out.println("Fecha Formateada: "+sdf.format(c1.getTime()));
//        c1.add(Calendar.DATE, 1);
//        System.out.println("Fecha Formateada: "+sdf.format(c1.getTime()));
    /**
     * *
     * Metodo que permite trasfomar una fecha que esta en calendar a string
     *
     * @param fecha
     * @return
     */
    public String retornaFecha(Calendar fecha) {
        String retorno = null;
        SimpleDateFormat adf = new SimpleDateFormat("dd/MM/yyyy");
        if (fecha != null) {
            retorno = adf.format(fecha.getTime());
        }
        return retorno;
    }

    /**
     * *
     *Metodo que permite crear un calendar a partir de un string 
     * @param fecha
     * @return
     */

    public Calendar retornaCalendar(String fecha) {
        SimpleDateFormat adf = new SimpleDateFormat("dd/MM/yyyy");
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(adf.parse(fecha));
        } catch (Exception e) {

        }
        return cal;
    }

    
    /***
     * Metodo que retorna un string para la consulta a la base de datos
     * @param fecha
     * @return 
     */
    public String retornaFechaConsulta(Calendar fecha) {
        String retorno = null;
        SimpleDateFormat adf = new SimpleDateFormat("yyyy-MM-dd");
        if (fecha != null) {
            retorno = adf.format(fecha.getTime());
        }
        return retorno;
    }

    /**
     * Metodo que me permite validar cajas de texto
     * @param evt
     * @param txt
     * @param tam 
     */
     
    public void valNum(KeyEvent evt, JTextField txt, int tam) {

        try {
            char c = evt.getKeyChar();
            if (!Character.isDigit(c)) {
                evt.consume();
                Toolkit.getDefaultToolkit().beep();
            }
            if (txt.getText().length() == tam) {
                evt.consume();
                Toolkit.getDefaultToolkit().beep();
            }
            if (c == '\n') {
                ((JComponent) evt.getSource()).transferFocus();
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex + "Error");
        }
    }

    public void valLetra(KeyEvent evt, JTextField txt, int tam) {

        try {
            char c = evt.getKeyChar();
            if (Character.isDigit(c)) {
                evt.consume();
            }
            if (txt.getText().length() == tam) {
                evt.consume();
                Toolkit.getDefaultToolkit().beep();
            }
            if (c == '\n') {
                ((JComponent) evt.getSource()).transferFocus();
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex + "Error");
        }
    }

    public void valLetraNumero(KeyEvent evt, JTextField txt, int tam) {

        try {
            char c = evt.getKeyChar();
            if (!Character.isLetterOrDigit(c) && !Character.isWhitespace(c) && c != '@' && c != '.') {
                evt.consume();
            }
            if (txt.getText().length() == tam) {
                evt.consume();
                Toolkit.getDefaultToolkit().beep();
            }
            if (c == '\n') {
                ((JComponent) evt.getSource()).transferFocus();
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex + "Error");
        }
    }

    public void valMayuscula(KeyEvent evt, JTextField txt, int tam) {

        try {
            char letra = evt.getKeyChar();
            if ((letra >= 'A' && letra <= 'Z') || (letra == ' ')) {

            } else {
                evt.consume();
            }
            if (txt.getText().length() == tam) { // 2 es la cantidad de caracteres permitidos
                evt.consume();
                Toolkit.getDefaultToolkit().beep();
            }
            if (letra == '\n') {
                ((JComponent) evt.getSource()).transferFocus();
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex + "Error");
        }
    }

    public void valNumReal(KeyEvent evt, JTextField txt, int tam) {
        try {
            char c = evt.getKeyChar();

            if (((c < '0') || (c > '9')) && (c != KeyEvent.VK_BACK_SPACE)
                    && (c != '.')) {
                evt.consume();
            }
            if (c == '.' && txt.getText().contains(".")) {
                evt.consume();
            }
            if (txt.getText().length() == tam) { // 2 es la cantidad de caracteres permitidos
                evt.consume();
                Toolkit.getDefaultToolkit().beep();
            }
            if (c == '\n') {
                ((JComponent) evt.getSource()).transferFocus();
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex + "Error");
        }
    }

    public String codi(int id, int nro) {
        String num = "";
        if (id < 10) {
            num = "0" + String.valueOf(id);
        } else {
            num = String.valueOf(id);
        }
        nro++;
        if (nro < 10) {
            num += "000" + String.valueOf(nro);
        } else if (nro < 100) {
            num += "00" + String.valueOf(nro);
        } else if (nro < 1000) {
            num += "0" + String.valueOf(nro);
        } else {
            num += String.valueOf(nro);
        }
        return num;
    }

    public String obtenerStringCod(int c) {
        String cdd = "000001";
        if (c < 10) {
            cdd = "00000" + c;
        } else if (c < 100) {
            cdd = "0000" + c;
        } else if (c < 1000) {
            cdd = "000" + c;
        } else if (c < 10000) {
            cdd = "00" + c;
        } else if (c < 100000) {
            cdd = "0" + c;
        } else {
            cdd = "" + c;
        }
        return cdd;
    }

    public boolean verificaCedula(String cedula) {
        int tamaño = cedula.length();
        int suma = 0, inmediatosuperior, resp, digit;
        double inm;
        try {
            if (tamaño == 10) {
                for (int i = 0; i < 9; i++) {
                    int mult;
                    if (i % 2 == 0) {
                        mult = (Integer.valueOf(cedula.substring(i, i + 1))) * 2;
                        if (mult > 9) {
                            mult = mult - 9;
                        }
                        suma += mult;
                    } else {
                        suma += Integer.valueOf(cedula.substring(i, i + 1));
                    }
                }
                inm = (double) suma / 10;
                digit = suma % 10;

                if (digit > 0) {
                    inmediatosuperior = (int) (inm + 1) * 10;
                } else {
                    inmediatosuperior = (int) inm * 10;
                }

//                System.out.println("Inmediatodividido: " + inm);
//                inmediatosuperior = (int) Math.ceil(inm) * 10;
                resp = inmediatosuperior - suma;
                if (resp == Integer.valueOf(cedula.substring(9))) {
                    System.out.println("Cedula Correcta");
                    return true;
                } else {
                    System.out.println("Cedula Incorrecta");
                }
            } else {
                System.out.println("Ingrese un número de 10 digitos");
            }

        } catch (NumberFormatException e) {
            System.out.println("Error " + e);
        }
        return false;
    }
}
