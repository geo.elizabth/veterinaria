/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import Modelo.Persona;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import conexion.Conexion;
import java.util.ArrayList;
import java.util.List;
import modelo.Cuenta;
import modelo.Rol;

/**
 *
 * @author Eli
 */
public class PersonaDB extends Conexion {

    public boolean registrarUsuario(Persona per, Cuenta cuenta) {
        PreparedStatement ps = null;
        PreparedStatement ps1 = null;
        Connection con = getConexion();
        String sql = "INSERT INTO Persona (cedula, nombre,apellido,direccion,telefono,email,rol_id) VALUES(?,?,?,?,?,?,?)";
        String sql1 = "INSERT INTO Cuenta (usuario,clave,estado,persona_id) VALUES(?,?,?,(select id_persona from Persona order by id_persona desc limit 1))";
        try {
            ps = con.prepareStatement(sql);
            ps1 = con.prepareStatement(sql1);
            ps.setString(1, per.getCedula());
            ps.setString(3, per.getNombre());
            ps.setString(2, per.getApellido());
            ps.setString(4, per.getDireccion());
            ps.setString(5, per.getTelefono());
            ps.setString(6, per.getEmail());
            ps.setInt(7, per.getId_rol());

            ps1.setString(1, cuenta.getUsuario());
            ps1.setString(2, cuenta.getClave());
            ps1.setBoolean(3, cuenta.getEstado());
            //ps1.setBoolean(3, cuenta.getEstado());
            int n = ps.executeUpdate();
            if (n != 0) {
                int n2 = ps1.executeUpdate();
                if (n2 != 0) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (SQLException e) {
            System.err.println(e);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
    }

    public boolean modificarUsuario(Persona per, Cuenta cuen) {
        PreparedStatement ps = null;
        PreparedStatement ps1 = null;
        Connection con = getConexion();
        String sql = "UPDATE Persona SET apellido=?, nombre=?, direccion=?, telefono=?, email=?, rol_id=? WHERE id_persona=? ";
        String sql1 = "UPDATE Cuenta SET usuario=?, clave=? WHERE persona_id=? ";
        try {
            ps = con.prepareStatement(sql);
            ps1 = con.prepareStatement(sql1);
            ps.setString(1, per.getApellido());
            ps.setString(2, per.getNombre());
            ps.setString(3, per.getDireccion());
            ps.setString(4, per.getTelefono());
            ps.setString(5, per.getEmail());
            ps.setInt(6, per.getId_rol());
            ps.setInt(7, per.getId_persona());

            ps1.setString(1, cuen.getUsuario());
            ps1.setString(2, cuen.getClave());
            ps1.setInt(3, per.getId_persona());

            int n = ps.executeUpdate();
            if (n != 0) {
                int n2 = ps1.executeUpdate();
                if (n2 != 0) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (SQLException e) {
            System.err.println(e);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
    }

    public boolean eliminar(Persona per) {
        PreparedStatement ps = null;
        Connection con = getConexion();
        String sql = "DELETE FROM Persona WHERE cedula=? ";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, per.getCedula());
            ps.execute();
            return true;
        } catch (SQLException e) {
            System.err.println(e);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
    }

    public Persona buscar(String cedula) {
        Persona per = null;
        PreparedStatement ps = null;
        ResultSet rs1 = null;
        Connection con = getConexion();
        String sql = "SELECT * FROM Persona WHERE cedula=? ";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, cedula);
            rs1 = ps.executeQuery();
            if (rs1.next()) {
                per = new Persona();
                per.setCedula(rs1.getString("cedula"));
                per.setApellido(rs1.getString("apellido"));
                per.setNombre(rs1.getString("nombre"));
                per.setDireccion(rs1.getString("direccion"));
                per.setTelefono(rs1.getString("telfono"));
                per.setCedula(rs1.getString("email"));
            }
        } catch (SQLException e) {
            System.err.println(e);
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
        return per;
    }

    public List<Persona> TraeUsuarios() {
        Persona per = null;
        List<Persona> ListaPer = new ArrayList<>();
        PreparedStatement ps1 = null;
        ResultSet rs1 = null;
        Connection con = getConexion();
        try {
            String sql1 = "Select * from Persona p inner join Cuenta u on p.id_persona=u.persona_id ";
            ps1 = con.prepareStatement(sql1);
            rs1 = ps1.executeQuery();
            while (rs1.next()) {
                per = new Persona();
                per.setId_persona(rs1.getInt("id_persona"));
                per.setCedula(rs1.getString("cedula"));
                per.setApellido(rs1.getString("apellido"));
                per.setNombre(rs1.getString("nombre"));
                per.setDireccion(rs1.getString("direccion"));
                per.setTelefono(rs1.getString("telefono"));
                per.setEmail(rs1.getString("email"));
                per.setId_rol(rs1.getInt("rol_id"));
                ListaPer.add(per);
            }
        } catch (SQLException e) {
            System.err.println(e);
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
        return ListaPer;
    }

    public List<Rol> TraeRol() {
        Rol rol = null;
        List<Rol> ListaRol = new ArrayList<>();
        PreparedStatement ps1 = null;
        ResultSet rs1 = null;
        Connection con = getConexion();
        try {
            String sql1 = "Select * from Rol";
            ps1 = con.prepareStatement(sql1);
            rs1 = ps1.executeQuery();
            while (rs1.next()) {
                rol = new Rol();
                rol.setId_rol(rs1.getInt("id_rol"));
                rol.setNombre(rs1.getString("nombre"));
                rol.setEstado(rs1.getBoolean("estado"));
                ListaRol.add(rol);
            }
        } catch (SQLException e) {
            System.err.println(e);
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
        return ListaRol;
    }

    /**
     * *
     * Metodo para buscar persona en la caja de texto
     */
    public List<Persona> buscarUsuario(String texto) {
        Persona per = null;
        List<Persona> ListaPer = new ArrayList<>();
        PreparedStatement ps1 = null;
        ResultSet rs1 = null;
        Connection con = getConexion();
        try {
            texto = texto.trim();
            if (texto.length() == 0) {
                texto = "";
            } else {
                texto = "" + texto + "%";
            }
            String sql1 = "from Persona where cedula like '" + texto + "' or apellido like '" + texto + "' or nombbre like '" + texto + "' or telefono like '" + texto + "'  or direccion like '" + texto + "' ";
            ps1 = con.prepareStatement(sql1);
            rs1 = ps1.executeQuery();
            while (rs1.next()) {
                per = new Persona();
                per.setId_persona(rs1.getInt("id_persona"));
                per.setCedula(rs1.getString("cedula"));
                per.setApellido(rs1.getString("apellido"));
                per.setNombre(rs1.getString("nombre"));
                per.setDireccion(rs1.getString("direccion"));
                per.setTelefono(rs1.getString("telefono"));
                per.setEmail(rs1.getString("email"));
                per.setId_rol(rs1.getInt("rol_id"));
                ListaPer.add(per);
            }
        } catch (Exception e) {
            System.out.println(e);
            return new ArrayList<Persona>();
        }
        return new ArrayList<Persona>();
    }

    public Persona personaUsuario(int id_persona) {
        Persona per = null;
        PreparedStatement ps = null;
        ResultSet rs1 = null;
        Connection con = getConexion();
        String sql = "Select * from Persona p inner join Cuenta u on p.id_persona=u.persona_id where id_persona='" + id_persona + "'";
        try {
            ps = con.prepareStatement(sql);
            rs1 = ps.executeQuery();
            if (rs1.next()) {
                per = new Persona();
                per.setId_persona(rs1.getInt("id_persona"));
                per.setCedula(rs1.getString("cedula"));
                per.setCedula(rs1.getString("cedula"));
                per.setApellido(rs1.getString("apellido"));
                per.setNombre(rs1.getString("nombre"));
                per.setDireccion(rs1.getString("direccion"));
                per.setTelefono(rs1.getString("telefono"));
                per.setEmail(rs1.getString("email"));
                per.setId_rol(rs1.getInt("rol_id"));
            }
        } catch (SQLException e) {
            System.err.println(e);
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
        return per;
    }

    public Cuenta personaCuenta(int id_persona) {
        Cuenta per = null;
        PreparedStatement ps = null;
        ResultSet rs1 = null;
        Connection con = getConexion();
        String sql = "Select * from Cuenta where persona_id='" + id_persona + "'";
        try {
            ps = con.prepareStatement(sql);
            rs1 = ps.executeQuery();
            if (rs1.next()) {
                per = new Cuenta();
                per.setId_cuenta(rs1.getInt("id_cuenta"));
                per.setUsuario(rs1.getString("usuario"));
                per.setClave(rs1.getString("clave"));
            }
        } catch (SQLException e) {
            System.err.println(e);
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
        return per;
    }

    /**
     * Metodos que perteecen a cliente
     * @param per
     * @return 
     */
     public boolean registrarCliente(Persona per) {
        PreparedStatement ps = null;
        Connection con = getConexion();
        String sql = "INSERT INTO Persona (cedula, nombre,apellido,direccion,telefono,email,rol_id) VALUES(?,?,?,?,?,?,?)";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, per.getCedula());
            ps.setString(3, per.getNombre());
            ps.setString(2, per.getApellido());
            ps.setString(4, per.getDireccion());
            ps.setString(5, per.getTelefono());
            ps.setString(6, per.getEmail());
            ps.setInt(7, per.getId_rol());
            ps.execute();
            return true;
        } catch (SQLException e) {
            System.err.println(e);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
    }

    public boolean modificarCliente(Persona per) {
        PreparedStatement ps = null;
        Connection con = getConexion();
        String sql = "UPDATE Persona SET apellido=?, nombre=?, direccion=?, telefono=?, email=?, rol_id=? WHERE id_persona=? ";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, per.getApellido());
            ps.setString(2, per.getNombre());
            ps.setString(3, per.getDireccion());
            ps.setString(4, per.getTelefono());
            ps.setString(5, per.getEmail());
            ps.setInt(6, per.getId_rol());
            ps.setInt(7, per.getId_persona());
            ps.execute();
            return true;
        } catch (SQLException e) {
            System.err.println(e);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
    }

    public boolean eliminarCliente(Persona per) {
        PreparedStatement ps = null;
        Connection con = getConexion();
        String sql = "DELETE FROM Persona WHERE cedula=? ";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, per.getCedula());
            ps.execute();
            return true;
        } catch (SQLException e) {
            System.err.println(e);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
    }

    public Persona buscarCliente(int id_persona) {
        Persona per = null;
        PreparedStatement ps = null;
        ResultSet rs1 = null;
        Connection con = getConexion();
        String sql = "Select * from Persona where id_persona='" + id_persona + "'";
        try {
            ps = con.prepareStatement(sql);
            rs1 = ps.executeQuery();
            if (rs1.next()) {
                per = new Persona();
                per.setId_persona(rs1.getInt("id_persona"));
                per.setCedula(rs1.getString("cedula"));
                per.setCedula(rs1.getString("cedula"));
                per.setApellido(rs1.getString("apellido"));
                per.setNombre(rs1.getString("nombre"));
                per.setDireccion(rs1.getString("direccion"));
                per.setTelefono(rs1.getString("telefono"));
                per.setEmail(rs1.getString("email"));
                per.setId_rol(rs1.getInt("rol_id"));
            }
        } catch (SQLException e) {
            System.err.println(e);
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
        return per;
    }
    
     public Persona buscarPersona(String cedula) {
        Persona per = null;
        PreparedStatement ps = null;
        ResultSet rs1 = null;
        Connection con = getConexion();
        String sql = "Select * from Persona where cedula='" + cedula + "'";
        try {
            ps = con.prepareStatement(sql);
            rs1 = ps.executeQuery();
            if (rs1.next()) {
                per = new Persona();
                per.setId_persona(rs1.getInt("id_persona"));
                per.setCedula(rs1.getString("cedula"));
                per.setCedula(rs1.getString("cedula"));
                per.setApellido(rs1.getString("apellido"));
                per.setNombre(rs1.getString("nombre"));
                per.setDireccion(rs1.getString("direccion"));
                per.setTelefono(rs1.getString("telefono"));
                per.setEmail(rs1.getString("email"));
                per.setId_rol(rs1.getInt("rol_id"));
            }
        } catch (SQLException e) {
            System.err.println(e);
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
        return per;
    }
    public List<Persona> TraeClientes() {
        int id_rol = 4;
        Persona per = null;
        List<Persona> ListaPer = new ArrayList<>();
        PreparedStatement ps = null;
        ResultSet rs1 = null;
        Connection con = getConexion();
        try {
            String sql = "Select * from Persona where rol_id='" + id_rol + "'";
            ps = con.prepareStatement(sql);
            rs1 = ps.executeQuery();
            while (rs1.next()) {
                per = new Persona();
                per.setId_persona(rs1.getInt("id_persona"));
                per.setCedula(rs1.getString("cedula"));
                per.setApellido(rs1.getString("apellido"));
                per.setNombre(rs1.getString("nombre"));
                per.setDireccion(rs1.getString("direccion"));
                per.setTelefono(rs1.getString("telefono"));
                per.setEmail(rs1.getString("email"));
                ListaPer.add(per);
            }
        } catch (SQLException e) {
            System.err.println(e);
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
        return ListaPer;
    }
}
